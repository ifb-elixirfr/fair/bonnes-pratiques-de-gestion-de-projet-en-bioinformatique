1. Introduction générale
    1. Pourquoi ce guide ?
    2. Périmiètre du guide
2. Préparer son environnement de travail
    1. IDE
    2. Les outils qui nous simplifient la vie
        1. Sourcetree
3. Versionning
    1. Git 
    2. Forges
        1. Github
        2. GitLab
        3. Forge "maison"
    3. Documents indispensables 
        1. LICENCE
        2. Code de conduite
        3. Citation file
    3. Changlog automatique
    4. Release automatique
    5. Créer un DOI et mettre à jour (Zenodo)
    6. Archiver le code (Software heritage)
4. Travail collaboratif
    1. Roadmap (Milestones / Issues)
        1. Proposer des templates d'Issue
    2. Kaban (Boards / Projects) 
    3. Changelog
    4. Protéger les branches
        1. Interdiction de push sur main
        2. Blocage si pas de review
        3. Blocage par le CI (test de contruction de la doc)
5. Documentation
    1. Comment bien écrire un README
    2. Bien utiliser le wiki d'un répo
    3. Comment bien faire des issues
    4. Documenter son code
        1. Sphynx
        2. Bonnes pratiques d'écriture (nom de variables,...)
            1. R
            2. Python 
            3. Snakemake ? Nextflow ? 
            4. ?
6. Les environnements logiciels
    1. Conda
    2. Docker
    3. Singularity/Apptainer
    4. Module (spécificité d'un cluster)
7. CI/CD
    0. mettre en place un runner Gitlab 
        1. Pourquoi ? 
        2. Comment ? 
    1. CI
        1. Création d'une docker automatique
        2. Lint automatique
        3. Mettre en ligne un site web gratuitement (GitHub / GitLab pages)
            1. Un site HTML classique
            2. Mise en ligne d'un notebook (R, Jupyter et Quarto)
            3. Une documention construite (mkdoc)
        3. Génération de la doc comme une documention mkdoc (construction et mise en ligne)
        4. Coverage
        5. Changlog automatique
        6. Release automatique
    2. CD
        1. Ansible
        2. Registry (Ex Docker, les packages R, etc) ?
8. Workflow
    1. Snakemake
    2. Nextflow
    3. Airflow
9. Ressources de calcul
    1. Cluster
        1. Plan en cours (voir issue)
    2. Cloud
10. Notebook
    1. Rmarkdown
    2. Jupyter
    3. Quarto
11. Communication autour du projet
    1. Faire la pub de notre projet$
    2. Forum des utilisateurs (exemple pour Bird, Discussion, )
    3. Rendre accessible notre documentation (Wiki des répo, repo spécial pour la doc comme mkdoc + Github/GitLab pages)
12. Pour aller plus loin
    1. 
13. Postface 
    1. Et l'IA dans tout ça ? 
14. Conclusion
    1. 
