# Bonnes pratiques de gestion de projet en bioinformatique

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

## Foreword

Our guide is available :

- in french [here](https://ifb-elixirfr.gitlab.io/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/fr/)
- in english [here](https://ifb-elixirfr.gitlab.io/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/en/)

## Project layout

```text
mkdoc_env.yml.yml       # Conda env to build and test the site locally
README.md               # General readme 
mkdocs.yml              # The configuration file.
config/ 
    en/
        mkdocs.yml      # The configuration file : english part.
    fr/
        mkdocs.yml      # The configuration file: french part.
docs/
    en/
        index.md         # Index page   
        pages/
            chap1/  
                part1.md
                part2.md
                ...              # Other markdown pages, images and other files.
            chap2/  
                part1.md
                part2.md
                ...              # Other markdown pages, images and other files.
    fr/
        index.md         # Index page   
        pages/
            chap1/  
                part1.md
                part2.md
                ...              # Other markdown pages, images and other files.
            chap2/  
                part1.md
                part2.md
                ...              # Other markdown pages, images and other files.
overrides/
    assets/
        images/
            ...
```

## For collaborators and developers

This part is for collaborators and developers.

### Modify content

When you are in the repository, add and/or modify your markdown tutorials in the docs directory. The arborescence of the website menu is to setup in the mkdocs.yml file. 

**Please note: the guide is in French and English. It is therefore necessary to edit/modify both languages**

**Warning: In local mode, the language change buttons send you to the online pages. To switch from one language to another, change the URL directly.**

### Mkdocs

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Create and activate conda env

```bash
conda env create -f mkdoc_env.yml
conda activate mkdoc_env
```

3. Preview your project

```bash
mkdocs serve -f config/en/mkdocs.yml
```

Note : *The site can be accessed under http://localhost:8000/en/*

4. Add content

Read more at MkDocs [documentation](https://www.mkdocs.org/).

## Citation

If you use our guide, please cite us :

IFB-ElixirFr, Bonnes pratiques de gestion de projet en bioinformatique, (2023), GitLab repository, https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique

A DOI with Zenodo is comming.

## Contributors

* [Thomas Denecker](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* [Nadia Goué](https://gitlab.com/nagoue) <a itemprop="sameAs" content="https://orcid.org/0000-0003-2750-1473" href="https://orcid.org/0000-0003-2750-1473" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
* ...

## Contributing
Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of Conduct
Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## License

Our guide is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## Acknowledgement

- All contributors

## Ressources 

### mkdocs

- Official documentation : https://www.mkdocs.org/
- Material (best extension !) : https://squidfunk.github.io/mkdocs-material/
- Multi language structure : https://github.com/squidfunk/mkdocs-material/discussions/2346

### commit

- https://blog.stack-labs.com/code/devops_conventional_changelog/
- https://www.conventionalcommits.org/en/v1.0.0/
