# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.0](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/compare/v1.0.2...v1.1.0) (2023-08-07)


### Features

* new plan ([b1fcb8b](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/b1fcb8b580dc7e2d4b1ac9c8072f108fdeccbec6))

## [1.0.2](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/compare/v1.0.1...v1.0.2) (2023-06-05)


### Bug Fixes

* add .gitignore ([dbab5d9](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/dbab5d91a7cacc6ce7ea01f70293b387f3b6fa4e))

## [1.0.1](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/compare/v1.0.0...v1.0.1) (2023-06-05)


### Bug Fixes

* add_contributor ([b408231](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/b40823146e0255cdcd907a52dffa266c624793cc))

## 1.0.0 (2023-04-25)


### Features

* Add Licence & contributing files ([42abe44](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/42abe44ecac6b215382ca9dc4dd482cca748d9b0))
* Add programme in index ([0aee978](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/0aee978faa427b50ff922bb1460fb5fcd34f3de6))
* Auto changelog & release ([c7ad737](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/c7ad73770d287d65742cd2d6a316001d5d5a65ad))
* Create a demo page ([24f63fd](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/24f63fd12d5b052987a9de3d00400a35dbcbdfb9))
* Create mkdoc project ([394450f](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/394450ff2d650720e8e98b340a2ba7b4b8eb59fd))
* create multi language structure ([889f879](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/889f87929bef635b3a86e1ad3a14e6c46a1c36be))


### Bug Fixes

* add config in mkdoc ([0fead35](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/0fead350bd5504fc715bba72b47af47849d0e18f))
* add ide in nav ([959cf0b](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/959cf0bb685bb3ecc655dc297e5680f84db6e41c))
* site url ([6905b51](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/6905b5139e7d2880dabebe5a7e3a8ec1d80696bd))
* update CI file ([06f30f4](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/06f30f40926d7fb7cbf7e05c70366a35afec650a))
* update CI file ([3c839ba](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/3c839ba94b436be634e18d231cba616b2b647d8c))
* update conda env ([7e5ac8c](https://gitlab.com/ifb-elixirfr/fair/bonnes-pratiques-de-gestion-de-projet-en-bioinformatique/commit/7e5ac8caf8ddfbdf53bfa7cdee7105e2cfa8d712))
