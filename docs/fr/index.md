# Bienvenue !

## Pourquoi ce guide ? 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla ullamcorper aliquam. Nulla consequat sollicitudin elit, eget fermentum mauris facilisis non. Vestibulum accumsan lobortis magna et tincidunt. Vivamus a tellus at urna pharetra sollicitudin a non turpis. Nullam nibh lectus, rhoncus id urna eget, dictum ullamcorper turpis. Sed condimentum nulla vehicula lacinia efficitur. Nullam mattis, urna eget porttitor imperdiet, arcu dui facilisis mauris, ac euismod quam diam vel elit. Ut at gravida ipsum. Etiam aliquam viverra enim, et condimentum mi pellentesque sit amet. Integer ac pulvinar lorem, nec fringilla sem. Nam quis rutrum urna. Quisque eleifend pretium luctus.

Nulla facilisi. Sed dapibus lacus nec turpis viverra, eget tempus nulla faucibus. Morbi bibendum mi sed ornare feugiat. Praesent eleifend mauris et ante ultrices tincidunt eu ut eros. Cras eget magna et ex fermentum scelerisque. Nam sit amet tincidunt nulla, ut feugiat eros. Aliquam tincidunt, massa faucibus gravida pharetra, dui sapien luctus ante, sed faucibus lectus lacus vel purus. Donec non tortor laoreet, gravida orci feugiat, dignissim ipsum. Donec non lectus at dui consequat consequat. Donec metus justo, faucibus at magna a, sollicitudin interdum ipsum. Cras id vestibulum libero. Aenean accumsan arcu et ex lobortis tempus. Curabitur ullamcorper augue mattis orci fringilla ornare.

## Au programme dans ce guide

{{ read_table('./programme_fr.tsv', sep = ';') }}